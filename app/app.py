#Importando  flask y algunos paquetes
import pandas as pd
from flask import Flask, render_template, request, redirect, url_for, session
from datetime import date
from datetime import datetime

from funciones2 import listados

app = Flask(__name__)
from conexionBD import *  #Importando conexion BD
from funciones import *  #Importando mis Funciones
from routes import * #Vistas
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import ParameterGrid
from sklearn.inspection import permutation_importance
from sklearn.metrics import confusion_matrix
#from sklearn.metrics import plot_confusion_matrix
import multiprocessing
from sklearn.model_selection import KFold
from sklearn import tree
import seaborn as sns
sns.set(context="notebook", palette="Spectral", style = 'darkgrid' ,font_scale = 1.5, color_codes=True)
# Configuración warnings
# ==============================================================================
# No presentar advertencia
import warnings
warnings.filterwarnings('ignore')
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
plt.style.use('ggplot')
pd.set_option('display.max_columns',None)#Visualizamos todas las columnas
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('fast')
from keras.models import Sequential
from keras.layers import Dense,Activation,Flatten
from sklearn.preprocessing import MinMaxScaler
import re
import json
from werkzeug.security import generate_password_hash, check_password_hash
import sqlalchemy

data = pd.DataFrame()
data1 = pd.DataFrame()
data2 = pd.DataFrame()
pred = ""
y_test=""
opciones=""
opciones2=""
pred=""
X=""
y=""
path="static/data2.csv"

@app.route('/dashboard', methods=['GET', 'POST'])
def loginUser():
    conexion_SQLdb = connectionBD()
    if 'conectado' in session:
        return render_template('public/dashboard/data.html', dataLogin = dataLoginSesion())
    else:
        msg = ''
        if request.method == 'POST' and 'email' in request.form and 'password' in request.form:
            email      = str(request.form['email'])
            password   = str(request.form['password'])

            # Comprobando si existe una cuenta
            cursor = conexion_SQLdb.cursor(dictionary=True) #Se mantuvo la declaracion de variable
            cursor.execute("SELECT * FROM Login WHERE email = %s", [email]) #Se cambió el nombre de la tabla
            account = cursor.fetchone()

            if account:
                if check_password_hash(account['password'],password):
                    # Crear datos de sesión, para poder acceder a estos datos en otras rutas
                    session['conectado']        = True
                    session['id']               = account['id']
                    session['tipo_user']        = account['tipo_user']
                    session['nombre']           = account['nombre']
                    session['apellido']         = account['apellido']
                    session['email']            = account['email']
                    #session['sexo']             = account['sexo']
                    #session['pais']             = account['pais']
                    session['create_at']        = account['create_at']
                    #session['te_gusta_la_programacion']     = account['te_gusta_la_programacion']

                    msg = "Ha iniciado sesión correctamente."
                    msg = 'Datos incorrectos, por favor verfique!'
                    return render_template('public/modulo_login/index.html', msjAlert = msg, typeAlert=0)
            else:
                return render_template('public/modulo_login/login.html', msjAlert = msg, typeAlert=0)
    return render_template('public/modulo_login/login.html', msjAlert = 'Debe iniciar sesión.', typeAlert=0) #Se borró "dataPaises = listaPaises()" de la ultima parte de la linea

@app.route('/acceso', methods=['GET', 'POST'])
def authorization():
    conexion_SQLdb = connectionBD()
    email = str(request.form['email'])
    password = str(request.form['password'])

    # Comprobando si existe una cuenta
    cursor = conexion_SQLdb.cursor()
    #cursor = conexion_SQLdb.cursor(dictionary=True) #--->Antes con MySQL
    #print("Esto trae"+str(cursor))

    cursor.execute("SELECT * FROM TBL_Login WHERE email = ?", [email])
    #cursor.execute("SELECT * FROM Login WHERE email = %s", [email]) #--->Antes con MySQL
    account = cursor.fetchone()
    #print("Esto trae account: "+str(account))
    #row = account[5]
    #print("Esto trae row: " + row)

    #listados()  # Se reubicó para poder serguir utilizando las listas segun la tabla en SQL
    #print("Listas Generadas")

    if account:
        if check_password_hash(str(account[5]), password):
        #if check_password_hash(account['password'], password): #--->Antes con MySQL
            # Crear datos de sesión, para poder acceder a estos datos en otras rutas
            """session['conectado'] = True
            session['id'] = account['id']
            session['tipo_user'] = account['tipo_user']
            session['nombre'] = account['nombre']
            session['apellido'] = account['apellido']
            session['email'] = account['email']
            session['create_at'] = account['create_at'] #--->Antes con MySQL """

            session['conectado'] = True
            session['id'] = account[0]
            session['tipo_user'] = account[1]
            session['nombre'] = account[2]
            session['apellido'] = account[3]
            session['email'] = account[4]
            session['create_at'] = account[6]

            #print("------> Llegamos hasta aquí <------")

            response = {
                "message": "iniciaste sesion",
                "status": 1
            }
            return json.dumps(response)
        else:
            response = {
                "message": "contraseña incorrecta",
                "status": 0,
            }
        return json.dumps(response)
    else:
        response = {
            "message": "no existe cuenta",
            "status": 0,
        }
    return json.dumps(response)

@app.route('/generar', methods=['GET', 'POST'])
def generate():
    password_encriptada = generate_password_hash('147852', method='sha256')
    response = {
        "message": password_encriptada,
        "status": 1,
    }
    return json.dumps(response)

#Registrando una cuenta de Usuario
@app.route('/registro-usuario', methods=['GET', 'POST'])
def registerUser():
    msg = ''
    conexion_SQLdb = connectionBD()
    if request.method == 'POST':
        tipo_user                   = 3
        nombre                      = request.form['nombre']
        apellido                    = request.form['apellido']
        email                       = request.form['email']
        password                    = request.form['password']
        repite_password             = request.form['repite_password']
        #sexo                        = request.form['sexo']
        #te_gusta_la_programacion    = request.form['te_gusta_la_programacion']
        #pais                        = request.form['pais']
        create_at                   = date.today()
        #current_time = datetime.datetime.now()

        # Comprobando si ya existe la cuenta de Usuario con respecto al email
        cursor = conexion_SQLdb.cursor(dictionary=True)
        cursor.execute('SELECT * FROM Login WHERE email = %s', (email,))
        account = cursor.fetchone()
        cursor.close() #cerrrando conexion SQL
          
        if account:
            msg = 'Ya existe el Email!'
        elif password != repite_password:
            msg = 'Disculpa, las clave no coinciden!'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Disculpa, formato de Email incorrecto!'
        elif not email or not password or not password or not repite_password:
            msg = 'El formulario no debe estar vacio!'
        else:
            # La cuenta no existe y los datos del formulario son válidos,
            password_encriptada = generate_password_hash(password, method='sha256')
            conexion_SQLdb = connectionBD()
            cursor = conexion_SQLdb.cursor(dictionary=True)
            cursor.execute('INSERT INTO Login (tipo_user, nombre, apellido, email, password, create_at) VALUES (%s, %s, %s, %s, %s, %s)', (tipo_user, nombre, apellido, email, password_encriptada, create_at))
            conexion_SQLdb.commit()
            cursor.close()
            msg = 'Cuenta creada correctamente!'

        return render_template('public/modulo_login/index.html', msjAlert = msg, typeAlert=1) #Se borró "dataPaises = listaPaises()" de la ultima parte de la linea
    return render_template('public/layout.html', dataLogin = dataLoginSesion(), msjAlert = msg, typeAlert=0)



     
@app.route('/actualizar-mi-perfil/<id>', methods=['POST'])
def actualizarMiPerfil(id):
    if 'conectado' in session:
        msg = ''
        if request.method == 'POST':
            nombre        = request.form['nombre']
            apellido      = request.form['apellido']
            email         = request.form['email']
            #sexo          = request.form['sexo']
            #pais          = request.form['pais']

            if(request.form['password']):
                password         = request.form['password'] 
                repite_password  = request.form['repite_password'] 
                
                if password != repite_password:
                    msg ='Las claves no coinciden'
                    return render_template('public/dashboard/home.html', msjAlert = msg, typeAlert=0, dataLogin = dataLoginSesion())
                else:
                    nueva_password = generate_password_hash(password, method='sha256')
                    conexion_SQLdb = connectionBD()
                    cur = conexion_SQLdb.cursor()
                    cur.execute("""
                        UPDATE login_python 
                        SET 
                            nombre = %s, 
                            apellido = %s, 
                            email = %s,
                            password = %s
                        WHERE id = %s""", (nombre, apellido, email, nueva_password, id))
                    conexion_SQLdb.commit()
                    cur.close() #Cerrando conexion SQL
                    conexion_SQLdb.close() #cerrando conexion de la BD
                    msg = 'Perfil actualizado correctamente'
                    return render_template('public/dashboard/home.html', msjAlert = msg, typeAlert=1, dataLogin = dataLoginSesion())
            else:
                msg = 'Perfil actualizado con exito'
                conexion_SQLdb = connectionBD()
                cur = conexion_SQLdb.cursor()
                cur.execute("""
                    UPDATE login_python 
                    SET 
                        nombre = %s, 
                        apellido = %s, 
                        email = %s,
                    WHERE id = %s""", (nombre, apellido, email, id))
                conexion_SQLdb.commit()
                cur.close()
                return render_template('public/dashboard/home.html', msjAlert = msg, typeAlert=1, dataLogin = dataLoginSesion())
        return render_template('public/dashboard/home.html', dataLogin = dataLoginSesion())             
        
        
if __name__ == "__main__":
    data_entrenamiento=''
    data_validacion=''
    app.run(debug=True, port=8000)
    
    