import time

from flask import session
from conexionBD import *
from datetime import datetime
import pandas as pd
import numpy as np
import os
import keras
import matplotlib.pyplot as plt
import tensorflow as tf
from keras import applications, Sequential
from keras.utils import to_categorical
from keras.layers import Dense, GlobalAveragePooling2D, Convolution2D
from keras.layers.convolutional import Conv2D
from keras.layers import Dense, Dropout
from keras.layers.convolutional import MaxPooling2D
from keras.layers import Flatten
from keras.applications import MobileNet
from keras.preprocessing import image
from keras.applications.mobilenet import preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Model
from keras.optimizers import Adam
from mlxtend.evaluate import confusion_matrix
from keras import backend as K
import itertools

global modelo
global train_generator
global valid_generator
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import ParameterGrid
from sklearn.inspection import permutation_importance
from sklearn.metrics import confusion_matrix
# from sklearn.metrics import plot_confusion_matrix
import seaborn as sb
import multiprocessing
from sklearn.model_selection import KFold
from sklearn import tree
import seaborn as sns
from azure.identity import DefaultAzureCredential
from azure.storage.blob import BlobServiceClient, BlobClient, ContainerClient

sns.set(context="notebook", palette="Spectral", style='darkgrid', font_scale=1.5, color_codes=True)
import warnings

warnings.filterwarnings('ignore')
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.style.use('ggplot')
pd.set_option('display.max_columns', None)  # Visualizamos todas las columnas
import pandas as pd
import numpy as np
import matplotlib.pylab as plt

plt.rcParams['figure.figsize'] = (16, 9)
plt.style.use('fast')
from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from sklearn.preprocessing import MinMaxScaler

conexion_SQLdb = connectionBD()
cursor = conexion_SQLdb.cursor()
inicio_prediccion = time.time()


def ruta(archivo):
    import os
    target = archivo
    initial_dir = 'C:\\'
    path = ''
    for root, _, files in os.walk(initial_dir):
        if target in files:
            path = os.path.join(root, target)
            break
    return path


def data_1(archivo=""):
    global data, data1, path

    #if (session['email']=="gerencia@gmail.com") or (session['email']=="comercial@gmail.com"):path="static/data2.csv"
    #else:path=ruta(archivo)

#Para Azure
    #Inicio Login Azure Blob Storage
    #account_url = "https://spvmiabs.blob.core.windows.net"
    #default_credential = DefaultAzureCredential()

    # Create the BlobServiceClient object
    #blob_service_client = BlobServiceClient(account_url, credential=default_credential)
    #print("Nos logueamos con exito al Azure Blob Storage")
    #print("=======================\n")
    #Fin Login Azure Blob Storage

    #Pendiente transferencia de txt local a Blob Storage

#Fin Azure

    path = ruta(archivo)

    inicio_carga_total = time.time()

    inicio_delete = time.time()
    cursor.execute("TRUNCATE TABLE TBL_Data_Orig")
    conexion_SQLdb.commit()
    fin_delete = time.time()
    tiempo_delete = fin_delete - inicio_delete
    print("Se hizo el delete")
    print("Tiempo total del delete: " + str(round(tiempo_delete, 5)) + " segundos\n")

    # Ingresar data a tabla

    #Bulk Insert
    inicio_bulk = time.time()

    #Aqui me quede con este error:
    '''pyodbc.ProgrammingError: ('42000', '[42000] [Microsoft][ODBC Driver 18 for SQL Server][SQL Server]Cannot bulk load because 
    the file "C:\\Users\\quiqu\\OneDrive\\Escritorio\\SPVMIA\\Archivos Data\\Archivos txt\\TempVentas23022023_89k.txt" 
    could not be opened. Operating system error code 997(Overlapped I/O operation is in progress.). (4861) (SQLExecDirectW)')
    '''

    cursor.execute("BULK INSERT TBL_Data_Orig FROM " + "'" + path + "'" +
        " WITH (FIRSTROW = 1, DATAFILETYPE='char', FIELDTERMINATOR='\t', ROWTERMINATOR='0x0a')")
    conexion_SQLdb.commit()

    fin_bulk = time.time()
    tiempo_bulk = fin_bulk - inicio_bulk

    print("Se hizo el bulk")
    print("Tiempo total del bulk insert: " + str(round(tiempo_bulk, 5)) + " segundos\n")

    #---> Select con las columnas a trabajar identificando solo las filas que tengan valor (elimnando los nulos y vacios)
    inicio_carga_df = time.time()

    query = "SELECT cod_centro_estandar, desc_centro_estandar, cod_linea, desc_linea, cod_marca, desc_marca, fec_factura, " \
            "ABS(precio_venta_total) as precio_venta_total, ABS(precio_venta_unidad) as precio_venta_unidad, " \
            "ABS(pvp) as pvp, ABS(valor_neto_total) as valor_neto_total, ABS(valor_neto_unit) as valor_neto_unit, " \
            "ABS(igv_total) as igv_total, ABS(monto_total) as monto_total, ABS(costo_venta_total) as costo_venta_total," \
            "ABS(costo_venta_unit) as costo_venta_unit, year FROM TBL_Data_Orig ORDER BY fec_factura ASC"

    cursor.execute(query)
    conexion_SQLdb.commit()
    print("Se hizo el query del SELECT")

    data = pd.read_sql(query,conexion_SQLdb) #---> 1MM de registros
    print("Se ingresó info del Select al DF")

    fin_carga_df = time.time()
    tiempo_carga_df = fin_carga_df - inicio_carga_df
    print("Se hizo la carga df")
    print("Tiempo total de la carga df: " + str(round(tiempo_carga_df, 5)) + " segundos\n")

    inicio_dropna_df = time.time()
    data = data.dropna(axis=1)
    print("Terminó la eliminación de columnas que contienen valores faltantes")
    fin_dropna_df = time.time()
    tiempo_dropna_df = fin_dropna_df - inicio_dropna_df
    print("Se hizo la dropna df")
    print("Tiempo total de la dropna df: " + str(round(tiempo_dropna_df, 5)) + " segundos\n")

    inicio_carga_data = time.time()
    data = pd.DataFrame(data, columns=['cod_centro_estandar', 'desc_centro_estandar', 'cod_linea', 'desc_linea',
                                       'cod_marca', 'desc_marca', 'fec_factura', 'precio_venta_total',
                                       'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total',
                                       'monto_total', 'costo_venta_total', 'costo_venta_unit', 'year'])

    fin_carga_data = time.time()
    tiempo_carga_data = fin_carga_data - inicio_carga_data
    print("Se hizo la carga a data")
    print("Tiempo total de la carga a data: " + str(round(tiempo_carga_data, 5)) + " segundos\n")

    inicio_carga_data1 = time.time()
    data1 = pd.DataFrame(data, columns=['cod_centro_estandar', 'desc_centro_estandar', 'cod_linea', 'desc_linea',
                                        'cod_marca', 'desc_marca', 'fec_factura', 'precio_venta_total',
                                        'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit',
                                        'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit', 'year'])

    fin_carga_data1 = time.time()
    tiempo_carga_data1 = fin_carga_data1 - inicio_carga_data1
    print("Se hizo la carga a data1")
    print("Tiempo total de la carga a data1: " + str(round(tiempo_carga_data1, 5)) + " segundos\n")

    # Convertimos a tipos de datos
    inicio_conversion_datos_data = time.time()

    data['cod_centro_estandar'] = data['cod_centro_estandar'].astype('string')
    data['desc_centro_estandar'] = data['desc_centro_estandar'].astype('string')
    data['cod_linea'] = data['cod_linea'].astype('string')
    data['desc_linea'] = data['desc_linea'].astype('string')
    data['cod_marca'] = data['cod_marca'].astype('string')
    data['desc_marca'] = data['desc_marca'].astype('string')

    data.precio_venta_total = data.precio_venta_total
    data.precio_venta_unidad = data.precio_venta_unidad
    data.pvp = data.pvp
    data.valor_neto_total = data.valor_neto_total
    data.valor_neto_unit = data.valor_neto_unit
    data.igv_total = data.igv_total
    data.monto_total = data.monto_total
    data.costo_venta_total = data.costo_venta_total
    data.costo_venta_unit = data.costo_venta_unit
    data['fec_factura'] = pd.to_datetime(data['fec_factura'])
    data.fec_factura = pd.to_datetime(data.fec_factura, format="%d-%m-%Y", errors='coerce')

    fin_conversion_datos_data = time.time()
    tiempo_conversion_datos_data = fin_conversion_datos_data - inicio_conversion_datos_data
    print("Se hizo la conversion de datos a data")
    print("Tiempo total de la conversion de datos a data: " + str(round(tiempo_conversion_datos_data, 5)) + " segundos\n")

    inicio_conversion_datos_data1 = time.time()

    data1['cod_centro_estandar'] = data1['cod_centro_estandar'].astype('string')
    data1['desc_centro_estandar'] = data1['desc_centro_estandar'].astype('string')
    data1['cod_linea'] = data1['cod_linea'].astype('string')
    data1['desc_linea'] = data1['desc_linea'].astype('string')
    data1['cod_marca'] = data1['cod_marca'].astype('string')
    data1['desc_marca'] = data1['desc_marca'].astype('string')
    data1.precio_venta_total = data1.precio_venta_total
    data1.precio_venta_unidad = data1.precio_venta_unidad
    data1.pvp = data1.pvp
    data1.valor_neto_total = data1.valor_neto_total
    data1.valor_neto_unit = data1.valor_neto_unit
    data1.igv_total = data1.igv_total
    data1.monto_total = data1.monto_total
    data1.costo_venta_total = data1.costo_venta_total
    data1.costo_venta_unit = data1.costo_venta_unit
    data["year"] = data["fec_factura"].dt.year

    fin_conversion_datos_data1 = time.time()
    tiempo_conversion_datos_data1 = fin_conversion_datos_data1 - inicio_conversion_datos_data1
    print("Se hizo la conversion de datos a data1")
    print("Tiempo total de la conversion de datos a data1: " + str(round(tiempo_conversion_datos_data1, 5)) + " segundos\n")

    listados()
    print("path", path)
    print("======FIN DE CARGA==========")
    fin_carga_total = time.time()
    tiempo_carga_total = fin_carga_total - inicio_carga_total
    print("Tiempo total de carga total: " + str(round(tiempo_carga_total, 5)) + " segundos")

def listados():
    global data
    Listado_todos = pd.DataFrame()
    Listado_tiendas = pd.DataFrame()
    Listado_linea = pd.DataFrame() #EX Producto
    Listado_marca = pd.DataFrame()

    Listado_todos['columna'] = ['', 'Sin Filtro', 'Sin Filtro', 'Sin Filtro', 'Sin Filtro', 'Sin Filtro']

    Listado_tiendas = data.drop_duplicates(subset=['cod_centro_estandar', 'desc_centro_estandar'])
    Listado_tiendas = Listado_tiendas[['cod_centro_estandar', 'desc_centro_estandar']]
    Listado_tiendas['cod_centro_estandar'] = Listado_tiendas['cod_centro_estandar'].astype('string')
    Listado_tiendas['desc_centro_estandar'] = Listado_tiendas['desc_centro_estandar'].astype('string')
    Listado_tiendas = Listado_tiendas.dropna()
    Listado_tiendas.to_csv("static/files/Listado_tiendas.csv",sep=";")

    Listado_linea = data.drop_duplicates(subset=['cod_linea', 'desc_linea'])
    Listado_linea = Listado_linea[['cod_linea', 'desc_linea']]
    Listado_linea['cod_linea'] = Listado_linea['cod_linea'].astype('string')
    Listado_linea['desc_linea'] = Listado_linea['desc_linea'].astype('string')
    Listado_linea = Listado_linea.dropna()
    Listado_linea.to_csv("static/files/Listado_linea.csv",sep=";")

    Listado_marca = data.drop_duplicates(subset=['cod_marca', 'desc_marca'])
    Listado_marca = Listado_marca[['cod_marca', 'desc_marca']]
    Listado_marca['cod_marca'] = Listado_marca['cod_marca'].astype('string')
    Listado_marca['desc_marca'] = Listado_marca['desc_marca'].astype('string')
    Listado_marca = Listado_marca.dropna()
    Listado_marca.to_csv("static/files/Listado_marca.csv",sep=";")

def convertir(d):
    data['cod_centro_estandar'] = data['cod_centro_estandar'].astype('string') #Aqui se cambió
    data['desc_centro_estandar'] = data['desc_centro_estandar'].astype('string') #Aqui se cambió
    data['cod_linea'] = data['cod_linea'].astype('string')
    data['desc_linea'] = data['desc_linea'].astype('string')
    data['cod_marca'] = data['cod_marca'].astype('string')
    data['desc_marca'] = data['desc_marca'].astype('string')
    data.precio_venta_total = data.precio_venta_total
    data.precio_venta_unidad = data.precio_venta_unidad
    data.pvp = data.pvp
    data.valor_neto_total = data.valor_neto_total
    data.valor_neto_unit = data.valor_neto_unit
    data.igv_total = data.igv_total
    data.monto_total = data.monto_total
    data.costo_venta_total = data.costo_venta_total
    data.costo_venta_unit = data.costo_venta_unit

    data['fec_factura'] = pd.to_datetime(data['fec_factura'])
    data.fec_factura = pd.to_datetime(data.fec_factura, format="%d-%m-%Y", errors='coerce')
    data["year"] = data["fec_factura"].dt.year
    return data

'''Se va
def probar2(tipofiltro, buscar):
    global data1, data2, opciones, opciones2 ,X,y,path
    if (session['email']=="gerencia@gmail.com") or (session['email']=="comercial@gmail.com"):path="static/data2.csv"
    print(path)
#Se va'''

def probar(tipofiltro, buscar):

    global data1, data2, opciones, opciones2 ,X,y,path,data
    opciones = tipofiltro
    opciones2 = buscar
    print("x==", opciones, opciones2)
    posicion = opciones2.find('-')
    opciones2 = opciones2[0:posicion]
    print(opciones, "==", opciones2)

    '''
    #Comentar
        if (session['email']=="gerencia@gmail.com") or (session['email']=="comercial@gmail.com"):
            path="static/data2.csv"
            data_1()
        else:
    # Comentar
    '''

    query = "SELECT cod_centro_estandar, desc_centro_estandar, cod_linea, desc_linea, cod_marca, desc_marca, fec_factura, " \
            "ABS(precio_venta_total) as precio_venta_total, ABS(precio_venta_unidad) as precio_venta_unidad, " \
            "ABS(pvp) as pvp, ABS(valor_neto_total) as valor_neto_total, ABS(valor_neto_unit) as valor_neto_unit, " \
            "ABS(igv_total) as igv_total, ABS(monto_total) as monto_total, ABS(costo_venta_total) as costo_venta_total," \
            "ABS(costo_venta_unit) as costo_venta_unit, year FROM TBL_Data_Orig ORDER BY fec_factura ASC"

    cursor.execute(query)
    conexion_SQLdb.commit()
    # Se hizo el query de las listas

    data = pd.read_sql(query, conexion_SQLdb)

    data = data.dropna(axis=1)
    data = convertir(data)

    if opciones == "All":
        opciones2 = ""
        print("All")

        """
        data = pd.DataFrame(data1, columns=['cod_centro_estandar', 'desc_centro_estandar', 'cod_linea', 'desc_linea', 'cod_marca', 
                                            'desc_marca', 'fec_factura', 'precio_venta_total', 'precio_venta_unidad',
                                            'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total',
                                            'costo_venta_total', 'costo_venta_unit', 'AÑO'])
        """
        data = data.groupby(['fec_factura'])[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit']].sum()
        data = data.reset_index()
        print(data)

    if opciones == "Tienda":
        """
        data = pd.DataFrame(data1, columns=['cod_centro_estandar', 'desc_centro_estandar', 'cod_linea', 'desc_linea', 'cod_marca',
                                            'desc_marca', 'fec_factura', 'precio_venta_total', 'precio_venta_unidad',
                                            'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total',
                                            'costo_venta_total', 'costo_venta_unit', 'AÑO'])
        """
        data = data.groupby(['cod_centro_estandar', 'fec_factura'])[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit']].sum()
        data = data.reset_index()
        data = data[(data.cod_centro_estandar == opciones2)]

    if opciones == "Producto": #Linea
        print(opciones2)
        """
        data = pd.DataFrame(data1, columns=['cod_centro_estandar', 'desc_centro_estandar', 'cod_linea', 'desc_linea', 'cod_marca',
                                            'desc_marca', 'fec_factura', 'precio_venta_total', 'precio_venta_unidad',
                                            'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total',
                                            'costo_venta_total', 'costo_venta_unit', 'AÑO'])
        """
        data = data.groupby(['cod_linea', 'fec_factura'])[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit']].sum()
        data = data.reset_index()
        data = data[(data.cod_linea == opciones2)]

    if opciones == "Marca":
        """
        data = pd.DataFrame(data1, columns=['cod_centro', 'desc_centro1', 'cod_linea', 'desc_linea', 'cod_marca',
                                            'desc_marca', 'fec_factura', 'precio_venta_total', 'precio_venta_unidad',
                                            'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total',
                                            'costo_venta_total', 'costo_venta_unit', 'AÑO'])
        """
        data = data.groupby(['cod_marca', 'fec_factura'])[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit', 'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit']].sum()
        data = data.reset_index()
        data = data[(data.cod_marca == opciones2)]

    data2 = data[
        ['fec_factura', 'precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_total', 'valor_neto_unit',
         'igv_total', 'monto_total', 'costo_venta_total', 'costo_venta_unit']]

    data2["valor_neto_total"] = np.log(data["valor_neto_total"])
    data2['precio_venta_total1'] = data2['precio_venta_total'].shift(1)
    data2['precio_venta_total2'] = data2['precio_venta_total'].shift(2)
    data2['precio_venta_total3'] = data2['precio_venta_total'].shift(3)
    data2['precio_venta_total4'] = data2['precio_venta_total'].shift(4)
    data2['precio_venta_total5'] = data2['precio_venta_total'].shift(5)
    data2['precio_venta_total6'] = data2['precio_venta_total'].shift(6)
    data2['precio_venta_total7'] = data2['precio_venta_total'].shift(7)

    data2['precio_venta_unidad1'] = data2['precio_venta_unidad'].shift(1)
    data2['precio_venta_unidad2'] = data2['precio_venta_unidad'].shift(2)
    data2['precio_venta_unidad3'] = data2['precio_venta_unidad'].shift(3)
    data2['precio_venta_unidad4'] = data2['precio_venta_unidad'].shift(4)
    data2['precio_venta_unidad5'] = data2['precio_venta_unidad'].shift(5)
    data2['precio_venta_unidad6'] = data2['precio_venta_unidad'].shift(6)
    data2['precio_venta_unidad7'] = data2['precio_venta_unidad'].shift(7)

    data2['pvp1'] = data2['pvp'].shift(1)
    data2['pvp2'] = data2['pvp'].shift(2)
    data2['pvp3'] = data2['pvp'].shift(3)
    data2['pvp4'] = data2['pvp'].shift(4)
    data2['pvp5'] = data2['pvp'].shift(5)
    data2['pvp6'] = data2['pvp'].shift(6)
    data2['pvp7'] = data2['pvp'].shift(7)

    data2['valor_neto_total1'] = data2['valor_neto_total'].shift(1)
    data2['valor_neto_total2'] = data2['valor_neto_total'].shift(2)
    data2['valor_neto_total3'] = data2['valor_neto_total'].shift(3)
    data2['valor_neto_total4'] = data2['valor_neto_total'].shift(4)
    data2['valor_neto_total5'] = data2['valor_neto_total'].shift(5)
    data2['valor_neto_total6'] = data2['valor_neto_total'].shift(6)
    data2['valor_neto_total7'] = data2['valor_neto_total'].shift(7)

    data2['pvp1'] = data2['pvp'].shift(1)
    data2['pvp2'] = data2['pvp'].shift(2)
    data2['pvp3'] = data2['pvp'].shift(3)
    data2['pvp4'] = data2['pvp'].shift(4)
    data2['pvp5'] = data2['pvp'].shift(5)
    data2['pvp6'] = data2['pvp'].shift(6)
    data2['pvp7'] = data2['pvp'].shift(7)

    data2['valor_neto_unit1'] = data2['valor_neto_unit'].shift(1)
    data2['valor_neto_unit2'] = data2['valor_neto_unit'].shift(2)
    data2['valor_neto_unit3'] = data2['valor_neto_unit'].shift(3)
    data2['valor_neto_unit4'] = data2['valor_neto_unit'].shift(4)
    data2['valor_neto_unit5'] = data2['valor_neto_unit'].shift(5)
    data2['valor_neto_unit6'] = data2['valor_neto_unit'].shift(6)
    data2['valor_neto_unit7'] = data2['valor_neto_unit'].shift(7)

    data2['igv_total1'] = data2['igv_total'].shift(1)
    data2['igv_total2'] = data2['igv_total'].shift(2)
    data2['igv_total3'] = data2['igv_total'].shift(3)
    data2['igv_total4'] = data2['igv_total'].shift(4)
    data2['igv_total5'] = data2['igv_total'].shift(5)
    data2['igv_total6'] = data2['igv_total'].shift(6)
    data2['igv_total7'] = data2['igv_total'].shift(7)

    data2['monto_total1'] = data2['monto_total'].shift(1)
    data2['monto_total2'] = data2['monto_total'].shift(2)
    data2['monto_total3'] = data2['monto_total'].shift(3)
    data2['monto_total4'] = data2['monto_total'].shift(4)
    data2['monto_total5'] = data2['monto_total'].shift(5)
    data2['monto_total6'] = data2['monto_total'].shift(6)
    data2['monto_total7'] = data2['monto_total'].shift(7)

    data2['costo_venta_total1'] = data2['costo_venta_total'].shift(1)
    data2['costo_venta_total2'] = data2['costo_venta_total'].shift(2)
    data2['costo_venta_total3'] = data2['costo_venta_total'].shift(3)
    data2['costo_venta_total4'] = data2['costo_venta_total'].shift(4)
    data2['costo_venta_total5'] = data2['costo_venta_total'].shift(5)
    data2['costo_venta_total6'] = data2['costo_venta_total'].shift(6)
    data2['costo_venta_total7'] = data2['costo_venta_total'].shift(7)

    data2['costo_venta_unit1'] = data2['costo_venta_unit'].shift(1)
    data2['costo_venta_unit2'] = data2['costo_venta_unit'].shift(2)
    data2['costo_venta_unit3'] = data2['costo_venta_unit'].shift(3)
    data2['costo_venta_unit4'] = data2['costo_venta_unit'].shift(4)
    data2['costo_venta_unit5'] = data2['costo_venta_unit'].shift(5)
    data2['costo_venta_unit6'] = data2['costo_venta_unit'].shift(6)
    data2['costo_venta_unit7'] = data2['costo_venta_unit'].shift(7)

    # Definimos los features (x), es decir los datos que nos ayudan a predecir -  valor_neto_total
    X = data2[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_unit', 'igv_total', 'monto_total',
               'costo_venta_total', 'costo_venta_unit']]
    # Definimos label (y), es decir los datos de salida -  valor_neto_total
    y = data2[['valor_neto_total']]

    return opciones, opciones2

def modelado1():
    global data2, pred, y_test,X,y

    X = data2[['precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_unit', 'igv_total', 'monto_total',
               'costo_venta_total', 'costo_venta_unit']]
    # Definimos label (y), es decir los datos de salida -  valor_neto_total
    y = data2[['valor_neto_total']]
    print(X)
    print(y)
    # Dividimos entre PrueBa y  test
    from sklearn.model_selection import train_test_split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=12345, shuffle=False)

    from sklearn.tree import DecisionTreeRegressor
    # Llamamos a la función
    mi_dt = DecisionTreeRegressor(random_state=12345)

    # Entrenamos nuestro modelo
    mi_dt.fit(X_train, y_train)

    # Hacemos las predicciones
    pred = mi_dt.predict(X_test)
    print("=======pred===========")
    print(pred)
    print("=======FIN DE MODELADO===========")

def evaluacion():
    global pred, y_test, opciones, opciones2
    pred = np.exp(pred)
    print(pred)
    ytest = np.exp(y_test)

    test2 = pd.DataFrame(ytest)
    test2["pred"] = pred
    test2["diferencia"] = abs(test2["valor_neto_total"] - test2["pred"])

    from sklearn import metrics

    def timeseries_evaluacion_metrica_func(y_true, y_pred):
        def mean_absolute_percentage_error(y_true, y_pred):
            y_true, y_pred = np.array(y_true), np.array(y_pred)
            return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

        print(
            "Ahora vamos a ver porcentualmente que parecidos son los \npesos de entrenamiento respecto a los pesos de predicción")
        print(f'R2 is : {metrics.r2_score(y_true, y_pred)}', end='\n\n')
        print("Prediccion en base a los criterios:", opciones, " ", opciones2)
        print("Del 100% de patrones, hemos identificado el:", round(metrics.r2_score(y_true, y_pred) * 100,2), "% Patrones",
              "hay un ", round(100 - (metrics.r2_score(y_true, y_pred) * 100),2), "% de patrones que no se han podido hallar")

    timeseries_evaluacion_metrica_func(test2["valor_neto_total"], test2["pred"])

    test2 = pd.DataFrame(ytest)
    test2["pred"] = pred

    test2["diferencia"] = abs(test2["valor_neto_total"] - test2["pred"])
    print("=======FIN DE EVALUACION===========")


def despliegue1():
    global test2
    red_neuronal()
    print("=======FIN DE DESPLIEGUE===========")

def red_neuronal():
    import matplotlib.pyplot as plt
    from sklearn.ensemble import RandomForestRegressor
    from sklearn.metrics import mean_squared_error
    from sklearn.model_selection import cross_val_score
    from sklearn.model_selection import train_test_split
    from sklearn.model_selection import RepeatedKFold
    from sklearn.model_selection import GridSearchCV
    from sklearn.model_selection import ParameterGrid
    from sklearn.inspection import permutation_importance
    from sklearn.metrics import confusion_matrix
    # from sklearn.metrics import plot_confusion_matrix
    import seaborn as sb
    import multiprocessing
    from sklearn.model_selection import KFold
    from sklearn import tree
    import seaborn as sns
    sns.set(context="notebook", palette="Spectral", style='darkgrid', font_scale=1.5, color_codes=True)
    import warnings
    warnings.filterwarnings('ignore')
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    plt.style.use('ggplot')
    pd.set_option('display.max_columns', None)  # Visualizamos todas las columnas
    import pandas as pd
    import numpy as np
    import matplotlib.pylab as plt
    plt.rcParams['figure.figsize'] = (16, 9)
    plt.style.use('fast')
    from keras.models import Sequential
    from keras.layers import Dense, Activation, Flatten
    from sklearn.preprocessing import MinMaxScaler
    global data,data2
    data = data2[['fec_factura', 'precio_venta_total']]
    print("red neuronal")
    print(data)
    #
    PASOS = 7
    # convertir series en aprendizaje supervisado
    def series_to_supervised(data, n_in=1, n_out=1, dropnan=True):
        """
        Enmarque una serie temporal como un conjunto de datos de aprendizaje supervisado
        Argumentos:
            data: Secuencia de observaciones como una lista o matriz NumPy.
            n_in:Número de observaciones de retraso como entrada (X).
            n_out: Número de observaciones como salida (y).
            dropnan: Boolean si descartar o no filas con valores NaN.
        Retorna:
            Pandas DataFrame de series enmarcadas para aprendizaje supervisado.
        """
        n_vars = 1 if type(data) is list else data.shape[1]  # devuelve 1 lista
        df = pd.DataFrame(data)
        cols, names = list(), list()

        for i in range(n_in, 0, -1):
            cols.append(df.shift(i))
            names += [('var%d(t-%d)' % (j + 1, i)) for j in range(n_vars)]

        for i in range(0, n_out):
            cols.append(df.shift(-i))
            if i == 0:
                names += [('var%d(t)' % (j + 1)) for j in range(n_vars)]
            else:
                names += [('var%d(t+%d)' % (j + 1, i)) for j in range(n_vars)]
        # todo junto
        agg = pd.concat(cols, axis=1)
        agg.columns = names
        # eliminar fila con valores NaN
        if dropnan:
            agg.dropna(inplace=True)
        return agg
    # cargar dataset - importes
    values = data2['precio_venta_total'].values

    # asegúrese de que todos los datos sean flotantes
    values = values.astype('float32')  # importes en flotantes
    print(values)
    # normalizar funciones
    scaler = MinMaxScaler(feature_range=(-1, 1))
    values = values.reshape(-1, 1)  # esto lo hacemos porque tenemos 1 sola dimension
    scaled = scaler.fit_transform(values)
    # enmarcar como aprendizaje supervisado scaled=146, PASOS=7, 1
    reframed = series_to_supervised(scaled, PASOS, 1)
    rows = len(reframed.axes[0])
    cols = len(reframed.axes[1])
    print("numero de filas: " + str(rows))
    print("numero de columnas: " + str(cols))
    print(reframed)
    #
    numero = len(values)

    """
    80% entrenamiento
    20% prueba 
    """

    # Dividir en entrenamiento y conjuntos de prueba
    values = reframed.values

    ### n_train_days =111
    n_train_days = int(numero * 0.8) #80% entrenamiento

    train = values[:n_train_days, :]  # 111 x 8 = 888
    # print(train.size) #888
    # print(train.shape) # (111, 8)

    test = values[n_train_days:, :]
    # print(test.shape) #(28, 8)
    # print(test.size)  #224

    # dividir en entrada y salida
    x_train, y_train = train[:, :-1], train[:, -1]
    # print(x_train.shape) # (111, 7)
    # print(y_train.shape) # (111,)

    x_val, y_val = test[:, :-1], test[:, -1]
    # print(x_val.shape) # (28, 7)
    # print(y_val.shape) # (7,)

    # remodelar la entrada para que sea 3D [muestras, intervalos de tiempo, características]
    x_train = x_train.reshape((x_train.shape[0], 1, x_train.shape[1]))
    x_val = x_val.reshape((x_val.shape[0], 1, x_val.shape[1]))
    print("numero:", numero, "entrenamiento==>", x_train.shape, y_train.shape, "prueba==>", x_val.shape, y_val.shape)
    """
    entrenamiento==> (111, 1, 7) (111,) prueba==> (28, 1, 7) (28,)
    """
    #
    def crear_modeloFF():
        # definimos el modelo
        model = Sequential()
        # PASOS=7 entradas , activation='tanh' por -1 y 1
        model.add(Dense(PASOS, input_shape=(1, PASOS), activation='tanh'))
        model.add(Flatten())
        model.add(Dense(1, activation='tanh'))
        # Ahora compilamos el modelo , indicamos el optimizador y la medidad del error
        model.compile(loss='mean_absolute_error', optimizer='Adam', metrics=['mse'])
        model.summary()
        return model
    #
    EPOCHS = 30 #Cantidad de dias a predecir, valor por default "30"
    model = crear_modeloFF()
    # history=model.fit(x_train,y_train,epochs=EPOCHS,validation_data=(x_val,y_val),batch_size=PASOS)
    history = model.fit(x_train, y_train, epochs=EPOCHS, validation_data=(x_val, y_val), batch_size=PASOS)
    #
    resultados = model.predict(x_val)
    print(len(resultados), "Dias")
    #
    compara = pd.DataFrame(np.array([y_val, [x[0] for x in resultados]])).transpose()
    compara.columns = ['real', 'prediccion']
    inverted = scaler.inverse_transform(compara.values)
    compara2 = pd.DataFrame(inverted)
    compara2.columns = ['real', 'prediccion']
    compara2['diferencia'] = compara2['real'] - compara2['prediccion']
    compara2.head()
    #
    data2 = data2.reset_index()
    #
    inicio = data2["fec_factura"].min()
    fin = data2["fec_factura"].max()
    ultimosDias = data2
    ultimosDias['fec_factura'] = pd.to_datetime(data2['fec_factura'])
    mask = (data2['fec_factura'] >= inicio) & (data2['fec_factura'] <= fin)
    ultimosDias = ultimosDias.loc[mask]
    #
    values = ultimosDias['precio_venta_total'].values
    values = values.astype('float32')
    # normalize features
    values = values.reshape(-1, 1)  # esto lo hacemos porque tenemos 1 sola dimension
    scaled = scaler.fit_transform(values)
    reframed = series_to_supervised(scaled, PASOS, 1)
    reframed.drop(reframed.columns[[7]], axis=1, inplace=True)
    reframed.head(7)
    #
    # ULTIMA COLUMNA UNA SEMANA
    values = reframed.values
    x_test = values[6:, :]
    x_test = x_test.reshape((x_test.shape[0], 1, x_test.shape[1]))
    print(x_test.shape)
    #
    def agregarNuevoValor(x_test, nuevoValor):
        for i in range(x_test.shape[2] - 1):
            x_test[0][0][i] = x_test[0][0][i + 1]
        x_test[0][0][x_test.shape[2] - 1] = nuevoValor
        return x_test
    #
    results = []
    for i in range(EPOCHS): #Cantidad de predicciones. Antes 31
        parcial = model.predict(x_test)
        results.append(parcial[0])
        x_test = agregarNuevoValor(x_test, parcial[0])
    #
    from datetime import timedelta
    """
    Ya casi lo tenemos… Ahora las predicciones están en el dominio del -1 al 1 
    y nosotros lo queremos en nuestra escala “real” de importe vendidas. 
    Entonces vamos a “re-transformar” los datos con el objeto “scaler” que creamos antes.
    """
    adimen = [x for x in results]
    inverted = scaler.inverse_transform(adimen)
    #
    prediccion = pd.DataFrame(inverted)
    prediccion.columns = ['pronostico']
    #print("Para ver que trae: ", prediccion)
    #
    fin = data2["fec_factura"].max() #Saca la ultima fecha de la columna
    Date_required = fin + timedelta(days=1)
    print("fecha inicial")
    print(Date_required)
    print("=====prediccion=====")
    print(prediccion)

    prediccion1 = prediccion
    prediccion1.columns = ['pronostico']
    prediccion1.insert(0, "fecha", "", allow_duplicates=False)

    file = open("static/files/pronostico.txt", "w") #Escribe en el txt las predicciones realizadas
    file.close()

    with open("static/files/pronostico.txt", "a") as o:
        o.write("fecha;valor"+ "\n")

    i = 0

    for fila in prediccion.pronostico:
        print(Date_required, "==", fila)
        #linea = Date_required.strftime("%d-%m-%Y") + ";" + str(format(fila))
        linea = Date_required.strftime("%d-%m-%Y") + ";" + str(format(fila, "0,.2f"))#--> Con separador de miles (,)

        #Nueva Linea
        #prediccion1.loc[i] = (Date_required.strftime("%d-%m-%Y"), round(fila,2))
        prediccion1.loc[i] = (Date_required.strftime("%d-%m-%Y"), format(fila, "0.2f"))#--> Sin separador de miles
        i = i+1
        #Fin de nueva linea

        Date_required = Date_required + timedelta(days=1)
        with open("static/files/pronostico.txt", "a") as o:
            o.write(linea + "\n")

    cursor.execute("TRUNCATE TABLE TBL_Prediccion")
    conexion_SQLdb.commit()

    insert_Tabla_Prediccion(prediccion1)
    print("Data ingresada a tabla prediccion")

    fin_prediccion = time.time()
    tiempo_prediccion_total = fin_prediccion - inicio_prediccion
    print("\n=========================")
    print("Tiempo total de prediccion: " + str(round(tiempo_prediccion_total/60, 5)) + " minutos")
    print("\n=========================")

def insert_Tabla_Prediccion(df1):
    for index, row in df1.iterrows():
        cursor.execute("INSERT INTO TBL_Prediccion (fecha, pronostico) values(?,?)", row.fecha, row.pronostico)
    conexion_SQLdb.commit()

def mapa_calor():
    import base64
    from io import BytesIO
    from flask import Flask
    import seaborn as sn
    import pandas as pd
    import matplotlib.pyplot as plt
    from pylab import savefig
    from matplotlib.figure import Figure
    # MAPA DE CALOR PARA valor_neto_total
    global data1
    # MAPA DE CALOR PARA valor_neto_total
    mapa = data1[
        ['valor_neto_total', 'precio_venta_total', 'precio_venta_unidad', 'pvp', 'valor_neto_unit', 'igv_total',
         'monto_total', 'costo_venta_total', 'costo_venta_unit']]
    # mapa= data2
    colormap = plt.cm.viridis
    plt.figure(figsize=(12, 12))
    plt.title('Correlacion de Variables', y=1.05, size=15)
    sb.heatmap(mapa.astype(float).corr(), linewidths=0.1, vmax=1.0, square=True, cmap=colormap, linecolor='white',
               annot=True)



