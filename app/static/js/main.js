// Pone clase active a las opciones de menu
const hostURL = window.location.protocol + "//" + window.location.host + "/";
const currentUrl = window.location.href;
console.log(hostURL);
function getNameURLWeb(){
  let sPath = window.location.pathname;
  let sPage = sPath.substring(sPath.lastIndexOf('/') + 1);
  return sPage;
}
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
let pageName = getNameURLWeb().replace(/\.[^/.]+$/, "")? getNameURLWeb().replace(/\.[^/.]+$/, "") : "inicio";
console.log(pageName);

// carga la libreria para ordenar las imagenes de proyectos
function loadMasonry(container) {
  new Masonry(container, {
    percentPosition: true,
  });
}
// funcion que ejecuta la libreria para ordenar imagenes solo cuando las imagenes cargaron
function imagesLoader(container, replace, order){
  let imgsloaded = imagesLoaded(container);
  imgsloaded.on( 'done', function(instance) {
    loadMasonry(container);
  });
  imgsloaded.on( 'fail', function(instance) {
    if (instance.hasAnyBroken) {
      let data = instance.images;
      data.forEach(image => {
        if (image.isLoaded == false) {
          let imageName = image.proxyImage.src.replace(hostURL,'');
          console.log(`Esta imagen no se cargó ${imageName}`);
        }
      });
      order? loadMasonry(container) : null;
    }
  });
  imgsloaded.on( 'progress', function(instance,image) {
    if (image.isLoaded == false) {
      replace? image.img.src = 'https://via.placeholder.com/450x600/?text=Imagen+no+cargada' : image.img.parentNode.parentNode.remove();
      //image.img.src = 'https://via.placeholder.com/450/?text=Imagen+no+cargada';
      //image.img.parentNode.parentNode.remove();
    }
  });
}

let btnsLoading = document.getElementsByClassName('btn-loading');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

document.querySelectorAll('.btn-loading').forEach(item => {
  item.addEventListener('click', event => {
    event.preventDefault();
    let href = event.currentTarget.getAttribute('href');
    let time = getRandomInt(10000,20000);
    console.log(href);
    Swal.fire({
      title: 'Atención se esta ejecutando una acción!',
      html: 'Cargando información...',
      timer: time,
      width: 800,
      timerProgressBar: true,
      allowOutsideClick:false,
      allowEscapeKey:false,
      allowEnterKey:false,
      customClass: {
        timerProgressBar: 'text-success',
      },
      didOpen: () => {
        Swal.showLoading()
      },
    }).then((result) => {
      console.log(href);
      if (result.dismiss === Swal.DismissReason.timer) {
        if(href != null){
          window.location.href = href;
        }
      }
    })
  })
});

// imprime todas la imagenes en la seccion proyectos
let p_container = document.getElementById('container-images');
if(!!p_container){
  data = [
    { image: "https://via.placeholder.com/300x450" },
    { image: "https://via.placeholder.com/600x250" },
    { image: "https://via.placeholder.com/400x300" },
    { image: "https://via.placeholder.com/640x480" },
    { image: "https://via.placeholder.com/250x430" },
    { image: "https://via.placeholder.com/400x450" },
    { image: "https://via.placeholder.com/500x450" },
    { image: "https://via.placeholder.com/600x450" }
  ]
  let projects = '';
  data.forEach(item => {
    projects += `<div class="col-12 col-sm-6 col-md-4 col-lg-3 p-2">
                  <div class="card shadow rounded-0">
                    <img src="${item.image}" class="card-img-top rounded-0" alt="${item.image}">
                  </div>
                </div>`;
  });
  
  p_container.innerHTML = projects;
  imagesLoader(p_container, false, true);
}

let images_affected = document.getElementById('affected');
const load_affected_images = async() => {
  let pagina = getParameterByName('pagina');
  try {
    const response = await fetch(`${hostURL}cargar_imagenes_${pagina}_afectadas`);
    
    images = '';
    // Si la respuesta es correcta
    if(response.status === 200){
      const data = await response.json();
      //console.log(data);

      data.forEach(item => {
        //console.log(item)

        images += `<div class="col-12 col-sm-6 col-md-4 col-lg-3 p-2">
                  <div class="card shadow rounded-0">
                    <img src="${item}" class="card-img-top rounded-0" alt="${item}">
                  </div>
                </div>`;
      });

    } else if(response.status === 401){
      console.log('No tienes permiso para acceder');
    } else if(response.status === 404){
      console.log('No existe la página');
    } else if(response.status === 500){
      console.log('Error en el servidor');
    } else {
      console.log('Hubo un error y no sabemos que paso');
    }
    images_affected.innerHTML = images;
    imagesLoader(images_affected, true, false);
  } catch(error){
    console.log(error);
  }
}

if(!!images_affected){
  load_affected_images();
  images_affected.addEventListener('click', (e) =>{
    let btn;
    if(e.target.tagName == 'IMG'){
      console.log(e.target.src)
      Swal.fire({
        imageUrl:`${e.target.src}`,
        showConfirmButton:false,
        width: `auto`,
        imageHeight: '100vh',
        focusClose:false,
        background:'transparent',
      })
    }
    
  });
}


let images_not_affected = document.getElementById('not_affected');
const load_not_affected_images = async() => {
  let pagina = getParameterByName('pagina');
  try {
    const response = await fetch(`${hostURL}cargar_imagenes_${pagina}_no_afectadas`);
    
    images = '';
    // Si la respuesta es correcta
    if(response.status === 200){
      const data = await response.json();
      //console.log(data);

      data.forEach(item => {
        //console.log(item)

        images += `<div class="col-12 col-sm-6 col-md-4 col-lg-3 p-2">
                  <div class="card shadow rounded-0">
                    <img src="${item}" class="card-img-top rounded-0" alt="${item}">
                  </div>
                </div>`;
      });

    } else if(response.status === 401){
      console.log('No tienes permiso para acceder');
    } else if(response.status === 404){
      console.log('No existe la página');
    } else if(response.status === 500){
      console.log('Error en el servidor');
    } else {
      console.log('Hubo un error y no sabemos que paso');
    }
    images_not_affected.innerHTML = images;
    imagesLoader(images_not_affected, true, false);
  } catch(error){
    console.log(error);
  }
}

if(!!images_not_affected){
  load_not_affected_images();
  images_not_affected.addEventListener('click', (e) =>{
    let btn;
    if(e.target.tagName == 'IMG'){
      console.log(e.target.src)
      Swal.fire({
        imageUrl:`${e.target.src}`,
        showConfirmButton:false,
        width: `auto`,
        imageHeight: '100vh',
        focusClose:false,
        background:'transparent',
      })
    }
    
  });
}


let images_validation = document.getElementById('validation');
const load_validation_images = async() => {
  try {
    const response = await fetch(`${hostURL}cargar_imagenes_validacion`);
    
    images = '';
    // Si la respuesta es correcta
    if(response.status === 200){
      const data = await response.json();
      //console.log(data);

      data.forEach(item => {
        //console.log(item)

        images += `<div class="col-12 col-sm-6 col-md-4 col-lg-3 p-2">
                  <div class="card shadow rounded-0">
                    <img src="${item}" class="card-img-top rounded-0" alt="${item}">
                  </div>
                </div>`;
      });

    } else if(response.status === 401){
      console.log('No tienes permiso para acceder');
    } else if(response.status === 404){
      console.log('No existe la página');
    } else if(response.status === 500){
      console.log('Error en el servidor');
    } else {
      console.log('Hubo un error y no sabemos que paso');
    }
    images_validation.innerHTML = images;
    imagesLoader(images_validation, true, false);
  } catch(error){
    console.log(error);
  }
}

if(!!images_validation){
  load_validation_images();
  images_validation.addEventListener('click', (e) =>{
    let btn;
    if(e.target.tagName == 'IMG'){
      console.log(e.target.src)
      Swal.fire({
        imageUrl:`${e.target.src}`,
        showConfirmButton:false,
        width: `auto`,
        imageHeight: '100vh',
        focusClose:false,
        background:'transparent',
      })
    }
    
  });
}

const loginForm = document.getElementById('loginForm');
const postLoginData = async (form) => {
  try {
    console.log(new FormData(form));
    let requestOptions = {
      method: 'POST',
      headers: {
        //"Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: new FormData(form),
    };

    const res = await fetch(loginForm.action, requestOptions)
    json = await res.json();

    if (!res.ok) throw {status: res.status, statusText: res.statusText, response: json};
    console.log(json);

      if (json.status) {
        Swal.fire({
              icon: 'success',
              title: 'Inicio de sesión',
              text: json.message,
            }).then(function() {
                window.location = `${hostURL}data`;
            });
      }else{
        Swal.fire({
              icon: 'error',
              title: 'Error inicio de sesión',
              text: json.message,
            })
      }

  } catch (error) {
    let statusText = error.statusText || "Ocurrio un error";
    console.log(error.status, statusText);
    console.log(error.status, error);
  }
}
if(!!loginForm){
  loginForm.onsubmit = (e) => {
    e.preventDefault();
    postLoginData(loginForm);
  }
}

const selectFilter = document.getElementById('selectFiltros');
const selectStore = document.getElementById('selectTienda');
const selectProduct = document.getElementById('selectProducto');
const selectEquipment = document.getElementById('selectEquipo');
const btnFilter = document.getElementById('btn-filter');

if(!!selectFilter){
  selectFilter.addEventListener("change", function(e){
    let selects = document.querySelectorAll(".control-select");
    selects.forEach((select) => {
      select.disabled = true;
      select.classList.add('d-none');
    });
    if(e.target.value == 'Tienda'){
      selectStore.classList.remove('d-none');
      selectStore.disabled = false;
    }else if(e.target.value == 'Producto'){
      selectProduct.classList.remove('d-none');
      selectProduct.disabled = false;
    }else if(e.target.value == 'Marca'){
      selectEquipment.classList.remove('d-none');
      selectEquipment.disabled = false;
    }else if(e.target.value == ''){
      btnFilter.classList.add('d-none');
    }
    btnFilter.classList.remove('d-none');
  });
}

function parseCSV(text) {
  // Obtenemos las lineas del texto
  let lines = text.replace(/\r/g, '').split('\n');
  return lines.map(line => {
    // Por cada linea obtenemos los valores
    let values = line.split(',');
    return values;
  });
}

if(!!selectStore){
  /// static/images/construir-web.png
  let textCsv = "/static/files/Listado_tiendas.csv";
  Papa.parse(textCsv, {
    download: true,
    header: true,
    delimiter: ";",
    skipEmptyLines: true,
    complete: function(results){
      //console.log(results)
      let options = '<option value="" selected>Seleccionar tienda</option>';
      let data = results.data;
      console.log(data.length);
      data.forEach(row => {
        //console.log(row.codigo)
        options += `<option value="${row.cod_centro_estandar}-${row.desc_centro_estandar}">${row.desc_centro_estandar}</option>`
      })
      selectStore.innerHTML = options;
    }
  })
}

if(!!selectProduct){
  /// static/images/construir-web.png
  let textCsv = "/static/files/Listado_linea.csv";
  Papa.parse(textCsv, {
    download: true,
    header: true,
    delimiter: ";",
    skipEmptyLines: true,
    complete: function(results){
      //console.log(results)
      let options = '<option value="" selected>Seleccionar linea</option>';
      let data = results.data;
      console.log(data.length);
      data.forEach(row => {
        //console.log(row.codigo)
        options += `<option value="${row.cod_linea}-${row.desc_linea}">${row.desc_linea}</option>`
      })
      selectProduct.innerHTML = options;
    }
  })
}

if(!!selectEquipment){
  /// static/images/construir-web.png
  let textCsv = "/static/files/Listado_marca.csv";
  Papa.parse(textCsv, {
    download: true,
    header: true,
    delimiter: ";",
    skipEmptyLines: true,
    complete: function(results){
      //console.log(results)
      let options = '<option value="" selected>Seleccionar marca</option>';
      let data = results.data;
      console.log(data.length);
      data.forEach(row => {
        //console.log(row.codigo)
        options += `<option value="${row.cod_marca}-${row.desc_marca}">${row.desc_marca}</option>`
      })
      selectEquipment.innerHTML = options;
    }
  })
}

const tbody = document.querySelector('table tbody');
if(!!tbody){
  let textCsv = "/static/files/pronostico.txt";
  Papa.parse(textCsv, {
    download: true,
    header: true,
    delimiter: ";",
    skipEmptyLines: true,
    complete: function(results){
      //console.log(results)
      let rows = '';
      let data = results.data;
      console.log(data.length);
      data.forEach(row => {
        //console.log(row.codigo)
        rows += `<tr class="text-center"><td>${row.fecha}</td><td>${row.valor}</td></tr>` //Formato Moneda
      })
      tbody.innerHTML = rows;
    }
  })
}

const filterForm = document.getElementById('filterForm');
const postFilterData = async (form) => {
  try {
    console.log(new FormData(form));
    let requestOptions = {
      method: 'POST',
      headers: {
        //"Content-Type": "application/json",
        "X-Requested-With": "XMLHttpRequest"
      },
      body: new FormData(form),
    };

    const res = await fetch(filterForm.action, requestOptions)
    json = await res.json();

    if (!res.ok) throw {status: res.status, statusText: res.statusText, response: json};
    console.log(json);
    Swal.fire({
      icon: 'success',
      title: 'Acción realizada con exito',
      text: 'El filtro se ejecuto sin problemas',
    })

  } catch (error) {
    let statusText = error.statusText || "Ocurrio un error";
    console.log(error.status, statusText);
    console.log(error.status, error);
  }
}
if(!!filterForm){
  filterForm.onsubmit = (e) => {
    e.preventDefault();
    postFilterData(filterForm);
  }
}

const uploadForm = document.getElementById('uploadForm');
const btnUpload = document.getElementById('btnUpload');
const iptFile = document.getElementById('iptFile');
  
if(!!uploadForm){
  formFile.addEventListener('change', function(){
    let file = document.querySelector('input[type=file]').files[0].name;
    iptFile.value = file;
  });
  uploadForm.onsubmit = (e) => {
    e.preventDefault();
    console.log("enviado");
    let time = getRandomInt(10000,40000);
    Swal.fire({
      title: 'Atención se esta ejecutando una acción!',
      html: 'Cargando información...',
      timer: time,
      width: 800,
      timerProgressBar: true,
      allowOutsideClick:false,
      allowEscapeKey:false,
      allowEnterKey:false,
      customClass: {
        timerProgressBar: 'text-success',
      },
      didOpen: () => {
        Swal.showLoading()
      },
    }).then((result) => {
      if (result.dismiss === Swal.DismissReason.timer) {
        uploadForm.submit();
      }
    })
  }
}