from flask import Flask, render_template, redirect, url_for, session, request


from funciones import *  #Importando mis Funciones
import json
import os
import time

from funciones2 import probar, data_1, evaluacion, modelado1, despliegue1

#Declarando nombre de la aplicación e inicializando, crear la aplicación Flask
app = Flask(__name__)
application = app


app.secret_key = '97110c78ae51a45af397be6534caef90ebb9b1dcb3380af008f90b23a5d1616bf19bc29098105da20fe'


#Redireccionando cuando la página no existe
@app.errorhandler(404)
def not_found(error):
    if 'conectado' in session:
        return redirect(url_for('inicio'))
    else:
        return render_template('public/modulo_login/index.html')
    
    
#Creando mi Decorador para el Home
@app.route('/')
def inicio():
    return render_template('public/modulo_login/index.html')
    
    
@app.route('/iniciar')
def login():
    if 'conectado' in session:
        return redirect(url_for('data'))
    else:
        return render_template('public/modulo_login/login.html')

#@app.route('/home')
#def data_home():
    #role=session['tipo_user']
    #if role==3:
    #   return redirect(url_for('filtros'))
    #if request.form:
    #    if "archivo" in request.form:
    #      archivo = request.form['archivo']
    #      #print(archivo)
    #      data_1(archivo)

#    return render_template('public/dashboard/data_home.html')

@app.route('/data', methods=['GET', 'POST'])
def data():
    role=session['tipo_user']
    if role==3:
        return redirect(url_for('filtros'))
    if request.form:
        if "archivo" in request.form:
          archivo = request.form['archivo']
          #print(archivo)
          data_1(archivo)

    return render_template('public/dashboard/data.html')

@app.route('/validacion')
def validacion():
    return render_template('public/dashboard/validacion.html')

@app.route('/pruebas')
def pruebas():
    return render_template('public/dashboard/pruebas.html')

@app.route('/procesamiento')
def procesamiento():
    return render_template('public/dashboard/procesamiento.html')

@app.route('/filtros')
def filtros():
    filtro = False
    if(request.args.get('filtro')):
      filtro = request.args.get('filtro')
    return render_template('public/dashboard/filtros.html', filtro = filtro)

@app.route('/filtros', methods=['GET', 'POST'])
def filtrar():
    tipofiltro = request.form['tipofiltro']
    if "buscar" in request.form:
      buscar = request.form['buscar']
    else:
      buscar = ''
    data23 = probar(tipofiltro,buscar)
    response = {
      "data23": data23,
      "status": 1,
    }
    return json.dumps(response)

@app.route('/modelado')
def modelado(): #MODELADO
    modelado1()
    return render_template('public/dashboard/procesamiento.html')

@app.route('/normalizacion')
def normalizacion():  #evaluacion
    evaluacion()
    return render_template('public/dashboard/procesamiento.html')

@app.route('/despliegue')
def despliegue():
    despliegue1()
    return render_template('public/dashboard/pronostico.html')

@app.route('/clasificacion')
def clasificacion():
    return render_template('public/dashboard/procesamiento.html')



# Cerrar session del usuario
@app.route('/logout')
def logout():
    msgClose = ''
    # Eliminar datos de sesión, esto cerrará la sesión del usuario
    session.pop('conectado', None)
    session.pop('id', None)
    session.pop('email', None)
    msgClose ="La sesión fue cerrada correctamente"
    return redirect(url_for('login'))